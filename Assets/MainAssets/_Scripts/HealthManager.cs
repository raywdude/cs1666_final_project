using UnityEngine;
using System.Collections;

public class HealthManager : MonoBehaviour {
	
	public float maxHealth = 100f;
	public float damage = 10f;
	public bool dead = false;
	
	private float health;
	
	void Awake() {
		
		health = maxHealth;
	}
		
	public void TakeDamage() {
		
		//Debug.Log("Taking Damage");
		health -= damage;
		if (health < 0) {
			dead = true;
		}
		
	}
	
	public void FirstAid(float tonic) {
		
		health += tonic;
		if (health > maxHealth) {
			health = maxHealth;
		}
	}
}
