using UnityEngine;
using System.Collections;

public class WeaponManager : MonoBehaviour {
	
	public float frequency = .1f;
	public float firingTimer = 0f;
	public bool oneShot = false;
	public Generator generator;
	public static WeaponManager Instance;
	public float range = 25f;
	
	private RaycastHit hitInfo;
	private bool firing = false;
	private Camera theCamera;

	void Awake () {
		
		//Debug.Log("The Weapon Manager has awoken!");
		Instance = this;
		theCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		if (theCamera == null) {
			Debug.Log("Couldn't find the camera");
		} else {
			Debug.Log("Found the camera");
		}
	}
	
	void Update () {
		
		if (Input.GetMouseButtonDown(0)) {
			StartFiring();
		}
		if (Input.GetMouseButtonUp(0)) {;
			StopFiring();
		}
		
		if (firing) {
			if (firingTimer > 0) {
				firingTimer -= Time.deltaTime;
			} else {
				Fire();
				firingTimer = frequency;
			}
		}

	}

	void StartFiring() {

		firing = true;
	}

	void StopFiring() {

		firing = false;
		firingTimer = 0f;
	}

	void Fire() {

		//Debug.Log("Firing one bullet");
		Ray ray = theCamera.ScreenPointToRay(Input.mousePosition);
		//if (Physics.Raycast(transform.position, transform.forward, out hitInfo)) {
		if (Physics.Raycast(ray, out hitInfo)) {
			GameObject target = hitInfo.collider.gameObject;
			if (hitInfo.collider.tag == "Generator") {
				generator = target.GetComponent<Generator>();
				if ( !generator.destroyed ) {
					generator.Hit();
				}
			} else if (hitInfo.collider.tag == "Monster") {
				Debug.Log("Hit a Monster");
				MonsterManager monster = target.GetComponent<MonsterManager>();
				if (monster == null) {
					monster = target.transform.parent.GetComponent<MonsterManager>();
					if (monster == null) {
						Debug.Log("Couldn't get monster manager");
						return;
					}
				}
				if ( !monster.destroyed ) {
					monster.Hit();
				}
			} else {
				Debug.Log("Hit " + hitInfo.collider.name);
			}
			if (oneShot) {
				StopFiring();
			}
		}
	}
}
