using UnityEngine;
using System.Collections;

public class Generator : MonoBehaviour {
	
	public string generatorName;
	public bool destroyed;
	public bool ready = false;
	public float frequency = 30f;
	public GameObject monster;
	
	//private Arriver arriver;
	protected HealthManager health;
	protected float genTimer;
	protected bool animating = false;
	protected Vector3 spawnForward = Vector3.forward * 30 +Vector3.up*9;
	
	void Awake () {
		
		//Debug.Log("Generator " + generatorName + " has awoken!");
		genTimer = 0f;
	}
	
	void Start () {
		
		health = transform.GetComponent<HealthManager>();
	}
	
	void Update () {
		
		if (ready) {
			Spawn();
		}
		
		if (destroyed) {
			DestroyGenerator();
		}
	}
	
	protected virtual void Spawn() {
		
		//Debug.Log("Using default spawn");
		if (genTimer > 0) {
			genTimer -= Time.deltaTime;
		} else {
			SpawnMonster();
			genTimer = frequency;
		}
	}
	
	protected virtual void DestroyGenerator() {
		
		//Debug.Log("Using default destroy monster");
		Destroy(gameObject);
	}
	
	public void Hit() {
		
		Debug.Log("Generator: I'm Hit");
		health.TakeDamage();
		if (health.dead) {
			Debug.Log("The Generator is Dead");
			destroyed = true;
			ready = false;
			animating = false;
		}
	}
	
	protected virtual void SpawnMonster() {
		
		Debug.Log("Spawning a monster");
		GameObject monster1 = Instantiate(monster, transform.position + spawnForward, Quaternion.identity) as GameObject;
		MonsterManager manager = monster1.GetComponent<MonsterManager>();
		if (manager == null) {
			Debug.Log("Couldn't get monster manager");
		} else {
			manager.generator = this;
		}
	}
}
