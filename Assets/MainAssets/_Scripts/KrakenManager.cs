using UnityEngine;
using System.Collections;

public class KrakenManager : MonsterManager {
	
	private GameObject monObj;
	private Animation monAnims;
	public float deathTimer = 10f;
	
	void Awake () {
	
		//Debug.Log("Monster tag is " + tag);
		character = GameObject.Find("TheController").gameObject;
		health = transform.GetComponent<HealthManager>();
		body = transform.GetComponent<Rigidbody>();
		monObj = transform.FindChild("Kraken").gameObject;
		if (monObj != null) {
			Debug.Log("Kraken Found!");
		}
		monAnims = monObj.GetComponent<Animation>();
		if (monAnims != null) {
			Debug.Log("Found Animations");
		}
	}
	
	void Start () {
		
		generator = KrakenGenerator.Instance;
	}
	
	protected override void BeIdle() {
		
		monAnims.Play("GiantKraken_Idle");		
	}
	
	protected override void DestroyMonster() {
		
		//Debug.Log("Animating is " + animating);
		if (animating) {
			if ( !monAnims.IsPlaying("GiantKraken_Death") ) {
				if (deathTimer > 0) {
					deathTimer -= Time.deltaTime;
				} else {
					Destroy(gameObject);
					animating = false;
				}
//			} else {
//				Debug.Log("Continuing Death Anim");
			}
		} else {
			Debug.Log("Starting Death Anim");
			monAnims.Play("GiantKraken_Death");
			animating = true;
		}
	}
	
	protected override void StartFiring() {
		
		if (animating) {
			if ( !monAnims.IsPlaying("GiantKraken_Attack") ) {
				Fire();
				firingTimer = frequency;
				animating = false;
			}
		} else {
			monAnims.Play("GiantKraken_Attack");
			animating = true;
		}
	}

	void Fire() {

		if (Physics.Raycast(transform.position, transform.forward, out hitInfo, range)) {
			//Debug.Log("Hit " + hitInfo.collider.name);
			if (hitInfo.collider.gameObject == character) {
				//Debug.Log("Hit the character!");
			}
		}
	}

}
