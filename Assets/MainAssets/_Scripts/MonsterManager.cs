using UnityEngine;
using System.Collections;

public class MonsterManager : MonoBehaviour {
	
	public GameObject character;
	public Generator generator;
	public float frequency = 5f;
	public float firingTimer = 0f;
	public float range = 10f;
	public float speed = 6f;
	public bool alert = false;
	public bool destroyed = false;
	
	protected HealthManager health;
	protected RaycastHit hitInfo;
	protected bool firing = false;
	protected Rigidbody body;
	protected bool inRange = false;
	protected bool animating = false;
	
	void Awake () {
	
		//Debug.Log("Monster tag is " + tag);
		character = GameObject.Find("TheController").gameObject;
		health = transform.GetComponent<HealthManager>();
		body = transform.GetComponent<Rigidbody>();
	}
	
	void Update () {
		
		if (generator != null && generator.ready) {
			alert = true;
		}
		
		if (generator != null && generator.destroyed) {
			firing = false;
		}
		
		if (destroyed) {
			DestroyMonster();
		} else {
			if (alert) {
				SearchForPlayer();
			}
			if (firing) {
				if (firingTimer <= 0) {
					StartFiring();
				}
			} else {
				animating = false;
				BeIdle();
			}
		}
		
		if (firingTimer > 0) {
			firingTimer -= Time.deltaTime;
		} else {
			firingTimer = 0f;
		}

	}
	
	protected virtual void BeIdle() {

	}
	
	protected virtual void StartFiring() {
		Fire();
		firingTimer = frequency;
	}
	
	protected virtual void DestroyMonster() {
		
		Destroy(gameObject);
	}
	
	protected virtual void SearchForPlayer() {

		Vector3 relativePos = character.transform.position - transform.position;
		inRange = relativePos.magnitude <= range;
		if ( !inRange ) {
			transform.position += relativePos*.001f;
		} else {
			if (Physics.Raycast(transform.position, transform.forward, out hitInfo, range)) {
				//Debug.Log("Hit " + hitInfo.collider.name);
				if (hitInfo.collider.gameObject == character) {
					//Debug.Log("I see the character!");
					firing = true;
				} else {
					firing = false;
				}
			}
		}
		Quaternion rotation = Quaternion.LookRotation(relativePos);
		transform.rotation = rotation;
		body.velocity = Vector3.zero;

	}

	void Fire() {

		if (Physics.Raycast(transform.position, transform.forward, out hitInfo, range)) {
			//Debug.Log("Hit " + hitInfo.collider.name);
			if (hitInfo.collider.gameObject == character) {
				//Debug.Log("Hit the character!");
			}
		}
	}
	
	public virtual void Hit() {
		
		Debug.Log("Monster: I'm Hit");
		health.TakeDamage();
		if (health.dead) {
			Debug.Log("The Monster is Dead");
			destroyed = true;
			animating = false;
		}
	}
}
