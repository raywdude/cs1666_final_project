using UnityEngine;
using System.Collections;

public class Transporter : MonoBehaviour {
	
	private GameObject character;
	public static Transporter Instance;

	void Awake () {
	
		//Debug.Log("The Transporter has awoken!");
		character = GameObject.Find("TheCharacter");
		Instance = this;
	}
	
	public void Transport(Arriver destination) {
		
		character.transform.position = destination.transform.position;
		character.active = false;
		destination.OnArrival();
	}
	
	public void RestartCharacter() {
		
		//Debug.Log("Restarting Character");
		character.active = true;
	}
}
