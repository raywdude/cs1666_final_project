using UnityEngine;
using System.Collections;

public class KrakenGenerator : Generator {
	
	public static KrakenGenerator Instance;
	private GameObject monGen;
	private Animation monAnims;
	private Light green1;
	private Light green2;
	private Light red1;
	private Light red2;

	void Awake () {
		
		Debug.Log("Generator " + generatorName + " has awoken!");
		Instance = this;
		monGen = transform.FindChild("MonsterGenerator").gameObject;
		if (monGen != null) {
			Debug.Log("MonsterGenerator Found!");
		}
		monAnims = monGen.GetComponent<Animation>();
		if (monAnims != null) {
			Debug.Log("Found Animations");
		}
	}
	
	void Start () {
		
		health = transform.GetComponent<HealthManager>();
		green1 = GameObject.Find("Green1").GetComponent<Light>();
		green2 = GameObject.Find("Green2").GetComponent<Light>();
		red1 = GameObject.Find("Red1").GetComponent<Light>();
		red2 = GameObject.Find("Red2").GetComponent<Light>();
		EnableLights(false);		
	}
	
	private void EnableLights(bool ok) {
	
		green1.enabled = ok;
		green2.enabled = ok;
		red1.enabled = ok;
		red2.enabled = ok;
	}
	
	protected override void Spawn() {
		
		//Debug.Log("Using my spawn");
		if (genTimer > 0) {
			genTimer -= Time.deltaTime;
		} else {
			if (animating) {
				if ( !monAnims.IsPlaying("MonsterGenerate") ) {
					SpawnMonster();
					genTimer = frequency;
					animating = false;
					EnableLights(false);
				}
			} else {
				monAnims.Play("MonsterGenerate");
				animating = true;
				EnableLights(true);
			}
		}
	}
	
	protected override void DestroyGenerator() {
		
		//Debug.Log("Using my destroy monster: animating is " + animating);
		if (animating) {
			if ( !monAnims.IsPlaying("MonsterExplode") ) {;
				animating = false;
				Destroy(gameObject);
			}
		} else {
			monAnims.Play("MonsterExplode");
			animating = true;
		}
	}
		
	protected override void SpawnMonster() {
		
		Debug.Log("Spawning a monster");
		GameObject monster1 = Instantiate(monster, transform.position + spawnForward - Vector3.up, transform.rotation) as GameObject;
		KrakenManager manager = monster1.GetComponent<KrakenManager>();
		if (manager == null) {
			Debug.Log("Couldn't get monster manager");
		} else {
			manager.generator = this;
		}
	}
}
