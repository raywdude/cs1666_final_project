using UnityEngine;
using System.Collections;

public class Token : MonoBehaviour {
	
	public bool retrieved = false;
	
	private bool inRange = false;

	void Awake() {
		
		inRange = false;
	}
	
	void Update () {
		
		if (inRange) {
			if (Input.GetKeyDown(KeyCode.E)) {
				Debug.Log("Retrieving Token");
				retrieved = true;
			}
		}
	}
	
	void OnTriggerEnter() {
		
		inRange = true;
	}

	void OnTriggerExit() {
	
		inRange = false;
	}
}
