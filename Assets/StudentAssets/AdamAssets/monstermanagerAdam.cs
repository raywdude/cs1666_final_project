using UnityEngine;
using System.Collections;

public class monstermanagerAdam : MonoBehaviour {
	
	public GameObject character;
	public Generator generator;
	public float frequency = 5f;
	public float firingTimer = 0f;
	public float range = 10f;
	public float speed = 6f;
	public bool alert = false;
	public bool destroyed = false;
	
	protected HealthManager health;
	protected RaycastHit hitInfo;
	protected bool firing = false;
	protected Rigidbody body;
	protected bool inRange = false;
	protected bool animating = false;
		
	void Awake () {
	
		//Debug.Log("Monster tag is " + tag);
		character = GameObject.Find("TheController").gameObject;
		health = transform.GetComponent<HealthManager>();
		body = transform.GetComponent<Rigidbody>();
	}
	
	void Update () {
		
		if (generator!=null && generator.ready) {
			alert = true;
		}
		
		if (generator!=null && generator.destroyed) {
			firing = false;
		}
		
		if (destroyed) {
			DestroyMonster();
		} else {
			if (alert) {
				SearchForPlayer();
			}
			if (firing) {
				if (firingTimer <= 0) {
					StartFiring();
				}
			} else {
				animating = false;
				BeIdle();
			}
		}
		
		if (firingTimer > 0) {
			firingTimer -= Time.deltaTime;
		} else {
			firingTimer = 0f;
		}

	}
	
		
	protected bool alreadyIdle=false;
	protected bool alreadyFiring=false;
	protected bool alreadyMove=false;
	
protected virtual void BeIdle() {
		/*
		if(!destroyed) 
		{
			if(!alreadyIdle && !animating) 
			{
				animation.Play("idle", PlayMode.StopAll);
				alreadyIdle = true;
			}
			else if(alreadyIdle && animating) 
			{
			alreadyIdle = false;
			}
		}
		*/


	}
	
	protected virtual void StartFiring() {
		if(!destroyed) 
		{
			if(!alreadyFiring && !animating) 
			{
				animation.CrossFade("attack", 0.2f);
				alreadyFiring = true;
				animating=true;
			}
			else if(alreadyFiring && animating) 
			{
			alreadyFiring = false;
			}
		}
		Fire();
		firingTimer = frequency;
	}
	
	
	protected virtual void SearchForPlayer() {
		
		CharacterController controller  = (CharacterController)GetComponent(typeof(CharacterController));
		Vector3 relativePos = character.transform.position - transform.position;
		inRange = relativePos.magnitude <= range*5;
		if(!inRange)
		{
			if (!firing) 
			{
				
			 	if(!destroyed) 
				{
					if(!alreadyMove) 
					{
						animation.CrossFade("move", 0.2f);
						alreadyMove = true;
						animating=true;
					}
					else if(alreadyMove && animating) 
					{
						alreadyMove = false;
					}
				}	
		    
				controller.Move(relativePos*.003f+Vector3.down);
			}
			
		} else {
			if (Physics.Raycast(transform.position, transform.forward, out hitInfo, range*5)) {
				//Debug.Log("Hit " + hitInfo.collider.name);
				if (hitInfo.collider.gameObject == character) {
					//Debug.Log("I see the character!");
					firing = true;
				} else {
					firing = false;
				}
			}
		}
		Quaternion rotation = Quaternion.LookRotation(relativePos);
		transform.rotation = rotation;
		body.velocity = Vector3.zero;

	}
	
	protected virtual void DestroyMonster() {
		
		Destroy(gameObject);
	}
	

	public void Fire() {

		if (Physics.Raycast(transform.position, transform.forward, out hitInfo, range*5)) {
			//Debug.Log("Hit " + hitInfo.collider.name);
			if (hitInfo.collider.gameObject == character) {
				//Debug.Log("Hit the character!");
			}
		}
	}
	
	public virtual void Hit() {
		
		Debug.Log("Monster: I'm Hit");
		health.TakeDamage();
		if (health.dead) {
			Debug.Log("The Monster is Dead");
			destroyed = true;
			animating = false;
		}
	}
}
