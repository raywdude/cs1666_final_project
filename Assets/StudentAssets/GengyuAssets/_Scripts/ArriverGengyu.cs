using UnityEngine;
using System.Collections;

public class ArriverGengyu : Arriver {
	/*public string arriverName;
	public int requiredLevels = 12;	
	public float arrivingDelay = 5f;
	
	private bool arriving = false;
	private Generator generator;
	private WeaponManager theWeapon;
	private Transporter transporter;
	public int levels = 0;
	public Material skybox;
	private ParticleEmitter emitter;
	private bool isMain;
	private float arrivingTimer;*/

	/*// Use this for initialization
	void Awake () {
	
		//Debug.Log("Arriver " + arriverName + " has awoken!");
		emitter = transform.particleEmitter;
		emitter.enabled = false;
		isMain = (arriverName == "Main");
	}
	
	void Start () {
		
		transporter = Transporter.Instance;
		theWeapon = WeaponManager.Instance;
	}
	
	void Update () {
		
		if (arriving) {
			if (arrivingTimer > 0) {
				arrivingTimer -= Time.deltaTime;
			} else {
				arrivingTimer = 0;
				arriving = false;
				transporter.RestartCharacter();
				emitter.enabled = false;
				emitter.ClearParticles();
				// Adjust level count
				if (isMain) {
					levels++;
				} else {
					generator.ready = true;
				}
				StartLevel();
			}
		}
	}*/
	
	// Override this method in your own named script to initialize your level
	public override void StartLevel() {
		skybox = new Material("Assets/StudentAssets/GengyuAssets/Materials/starfields_1024_2048_by_tbh_1138-d30dvm7.mat");
		//RenderSettings.ambientLight = new Color(34,41,44,255);
		Color some = new Color(0.8f, 0.8f, 0.8f, 0.8f);
		RenderSettings.ambientLight = some;
	}
	
	/*public void OnArrival() {
		
		arriving = true;
		arrivingTimer = arrivingDelay;
		emitter.enabled = true;
		Debug.Log("Arrived at " + arriverName);
		if ( !isMain ) {
			generator = GameObject.Find("Generator"+arriverName).GetComponent<Generator>();
			theWeapon.generator = generator;
		}
		if (skybox != null) {
			RenderSettings.skybox = skybox;
		}
	}*/

}
