using UnityEngine;
using System.Collections;

public class MonsterManagerGengyu : MonsterManager {
	public bool alreadyIdle = false;
	public bool alreadyAttacking = false;
	public bool alreadyMoving = false;
	
	protected override void BeIdle() {
		if(!destroyed) {
			if(!alreadyIdle && !animating) {
				animation.Play("idle", PlayMode.StopAll);
				alreadyIdle = true;
				//alreadyAttacking = alreadyMoving = false;
			}
			else if(alreadyIdle && animating) {
				alreadyIdle = false;	
			}
		}
		base.BeIdle();
	}
	
	protected override void StartFiring() {
		if(!destroyed) {
			if(!alreadyAttacking && firing) {
				animation.Play("attack", PlayMode.StopAll);
				alreadyAttacking = true;
				//alreadyIdle = alreadyMoving = false;
			}
			else if(alreadyAttacking && !firing) {
				alreadyAttacking = false;
			}
		}
		base.StartFiring();
	}
	
	protected override void SearchForPlayer() {
		if(!destroyed) {
			if(!alreadyMoving && alert) {
				animation.Play("move", PlayMode.StopAll);
				alreadyMoving = true;
				//alreadyIdle = alreadyAttacking = false;
			}
			else if(alreadyMoving && !alert) {
				alreadyMoving = false;
			}
		}
		base.SearchForPlayer();
	}
}

